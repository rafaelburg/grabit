package com.example.grabit;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.pm.PackageManager;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.io.IOException;

public class MainActivity extends AppCompatActivity {

    private MediaRecorder grabacion;
    private String archivoSalida = null;
    private Button btn_recorder;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // identificamos el boton de Rec
        btn_recorder = (Button) findViewById(R.id.btn_rec);


        // PERMISOS
        if (ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.RECORD_AUDIO}, 1000);
        }


    }


    public void Recorder(View view){

        // el objeto "grabacion" existe mientras se esta grabando el audio,
        // una vez que terminamos de grabar el audio se almacena en un archivo y el objeto se destruye
        // entonces , podemos comenzar una grabacion mientras el objeto "grabacion" es null
        if(grabacion == null){
            archivoSalida = Environment.getExternalStorageDirectory().getAbsolutePath() + "/grabit-02.mp3";

            grabacion = new MediaRecorder();

            // establecemos de donde queremos capturar el audio
            grabacion.setAudioSource(MediaRecorder.AudioSource.MIC);
            // definimos el formato del archivo de audio que se va a almacenar
            grabacion.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
            // tipo de codificacion
            grabacion.setAudioEncoder(MediaRecorder.OutputFormat.AMR_NB);

            // establecemos la ruta donde se va a almacenar
            grabacion.setOutputFile(archivoSalida);


            try{

                grabacion.prepare();
                grabacion.start();

            }catch(IOException e){

            }


            // cambiamos el texto del boton, de "REC" a "STOP"
            btn_recorder.setText("Stop");
            // lanzamos un mensaje Toast para indicar que estamos grabando
            Toast.makeText(getApplicationContext(),"Grabando...", Toast.LENGTH_SHORT).show();


        }else if(grabacion != null){
            grabacion.stop();
            grabacion.release();
            grabacion = null;

            btn_recorder.setText("Rec");
            Toast.makeText(getApplicationContext(),"Grabacion detenida", Toast.LENGTH_SHORT).show();

        }

    }


    public void reproducir(View view){
        MediaPlayer mediaPlayer = new MediaPlayer();

        try{

            mediaPlayer.setDataSource(archivoSalida);
            mediaPlayer.prepare();


        }catch (IOException e){

        }

        mediaPlayer.start();
        Toast.makeText(getApplicationContext(),"Reproduciendo audio...", Toast.LENGTH_SHORT).show();

    }

}
